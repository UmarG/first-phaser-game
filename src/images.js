import bombImg from './assets/bomb.png';
import dudeImg from './assets/dude.png';
import platformImg from './assets/platform.png';
import skyImg from './assets/sky.png';
import starImg from './assets/star.png';

export { bombImg, dudeImg, platformImg, skyImg, starImg };